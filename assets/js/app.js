const formatPhoneText = (value) => {
    value = value.replace(/-/g, '');

    if(value.length > 3 && value.length <= 6)
        value = value.slice(0,3) + "-" + value.slice(3);
    else if(value.length > 6)
        value = value.slice(0,3) + "-" + value.slice(3,6) + "-" + value.slice(6);

    return value;
};
let app = new Vue({
    el: '#app',
    data: {
        formatPhoneText: formatPhoneText,
        mainVideoMuted: true,
        trabajadorActual: '',
        currentPathname : '',
        cardData: {
            slides: [
                "assets/img/slides/slide1.jpg",
                "assets/img/slides/slide2.jpg",
                "assets/img/slides/slide3.jpg",
                "assets/img/slides/slide4.jpg",
                "assets/img/slides/slide5.jpg",
                "assets/img/slides/slide6.jpg",
                "assets/img/slides/slide7.png",
                "assets/img/slides/slide8.png",
                "assets/img/slides/slide9.jpg",
            ],
            datosTrabajador: {
                id: '',
                uuid: '',
                nombre: '',
                puesto: '',
                sucursal: '',
                telefonoTrabajo: '',
                extTrabajo: '',
                telefonoMobil: '',
                telefonoParticular: '',
                correo: '',
                paginaWeb: '',
                direccion: '',
                encTypeVCard: '',
                fotoVCard: '',
                fotoUrl: '',
                categoria: '',
                documentosUsuario: []
            },
            mostrarDetalles: false,
            seccionMostrada: 'detalles',
            documentoElegido: '',
            listaDocumentosCategoria: [],
            mostrarBtnDocumentos: true,
        },
        isPlayingIntro: false,
        logoGEMIVCardType: '',
        logoGEMIVCard: '',
        vCardCompanyName: '',
        documentosCategoria: {},
        documentoGenerales: [],
        reproducirAudio: false,
    },
    computed: {
        console: () => console,
        window: () => window,
    },
    created() {
        this.trabajadorActual = this.getUrlParam('trabajador');
        this.getCurrentPathname();
        this.getDataWorker();
        this.getConfigData();

    },
    mounted(){
    },
    methods: {
        getConfigData() {
            let $me = this;
            fetch('assets/json/config.json')
                .then( (r) => r.json() )
                .then( rs => {
                    let cnfVCard = rs['vCardConfig'];
                    if (cnfVCard) {
                        $me.logoGEMIVCardType  = cnfVCard['logoCardMIMEType'];
                        $me.logoGEMIVCard  = cnfVCard['logoCardBase64'];
                        $me.vCardCompanyName  = cnfVCard['organizationName'];
                    }
                    let docCommon = rs['documentos_comunes'];
                    if (docCommon) {
                        $me.documentoGenerales  = docCommon;
                    }

                    $me.reproducirAudio = rs['reproducir_audio'];
                });

            fetch('assets/json/documentos-categoria.json')
                .then( (r) => r.json() )
                .then( rs => {
                    $me.documentosCategoria  = rs;
                    this.getDocumentosCategoria();
                });
        },
        isCardID(){
            return /idcard/.test(  this.currentPathname.toLocaleLowerCase()  );
        },
        fillMeta(){
            document.querySelector('title').innerText = `Contacto - ${this.cardData.datosTrabajador.nombre}`;
            document.querySelector('meta[name=description]').innerText = `${this.cardData.datosTrabajador.puesto} ${this.cardData.datosTrabajador.sucursal}`;
            // Fancy
            document.querySelector('meta[property="fancy:name"]').innerText = `${this.cardData.datosTrabajador.nombre} - ${this.cardData.datosTrabajador.puesto} - ${this.cardData.datosTrabajador.sucursal}`;
            document.querySelector('meta[property="fancy:description"]').innerText = `${this.cardData.datosTrabajador.puesto} ${this.cardData.datosTrabajador.sucursal}`;
            //Canonical
            document.querySelector('link[rel=canonical]').innerText = `${location.href}`;
            document.querySelector('meta[property="og:url"]').innerText = `${location.href}`;
            //Facebook
            document.querySelector('meta[property="og:title"]').innerText = `${this.cardData.datosTrabajador.nombre} - ${this.cardData.datosTrabajador.puesto} - ${this.cardData.datosTrabajador.sucursal}`;
            document.querySelector('meta[property="og:description"]').innerText = `${this.cardData.datosTrabajador.puesto} ${this.cardData.datosTrabajador.sucursal}`;
            document.querySelector('meta[property="og:site_name"]').innerText = `Tarjeta Digital - ${this.cardData.datosTrabajador.nombre} `;
            //Twitter
            document.querySelector('meta[name="twitter:title"]').innerText = `${this.cardData.datosTrabajador.nombre} - ${this.cardData.datosTrabajador.puesto} - ${this.cardData.datosTrabajador.sucursal}`;
            document.querySelector('meta[name="twitter:description"]').innerText = `${this.cardData.datosTrabajador.puesto} ${this.cardData.datosTrabajador.sucursal}`;

        },
        getDocumentosCategoria(){
            if (this.cardData.datosTrabajador.uuid.length) {
                if ( this.documentosCategoria[this.cardData.datosTrabajador.categoria]  ) {
                    this.cardData.listaDocumentosCategoria = this.documentosCategoria[this.cardData.datosTrabajador.categoria].documentos;
                    this.cardData.mostrarBtnDocumentos =  this.documentosCategoria[this.cardData.datosTrabajador.categoria].mostrarDocumentos;
                }
            }
        },
        getDataWorker(){
            if ( this.trabajadorActual.length && this.isCardID() ) {
                let $me = this;
                fetch('assets/json/personal.json')
                    .then( (r) => r.json() )
                    .then( rs => {
                        let worker = rs.find( row => row.uuid === $me.trabajadorActual );
                        if (worker) {
                            $me.cardData.datosTrabajador = worker;
                            this.fillMeta();
                        }
                    });
            }

        },
        getCurrentPathname(){
            this.currentPathname = location.pathname;
        },
        getUrlVars() {
            let vars = {};
            let parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        },

        getUrlParam(parameter, defaultvalue = ''){
                let urlParameter = defaultvalue;
                if(window.location.href.indexOf(parameter) > -1){
                    urlParameter = this.getUrlVars()[parameter];
                }
                return urlParameter;
        },

        irUrl(ruta = '/') {
            location.href = `${ruta}?trabajador=${this.trabajadorActual}`;
        },

        /*****  main index  ******/
        onToggleMuteMainVideo() {
            this.mainVideoMuted = ! this.mainVideoMuted;
        },
        endMainVideo() {
            if ( ! this.trabajadorActual.trim().length ) {
                location.href = location.origin;
                return false;
            }
            else {
                this.irUrl('presentacion.html');
            }

        },


        /*****  IDCard  ******/
        onClickLogoInternational() {
            this.irUrl(location.origin);
        },
        generarVCard() {
            if (this.trabajadorActual) {
                let
                card = new vCard();
                card.firstName = this.cardData.datosTrabajador.nombre;
                card.email = this.cardData.datosTrabajador.correo;
                card.cellPhone = this.cardData.datosTrabajador.telefonoMobil;
                card.workPhone = this.cardData.datosTrabajador.telefonoTrabajo;
                card.homePhone = this.cardData.datosTrabajador.telefonoParticular;
                card.organization = this.vCardCompanyName;
                card.title = this.cardData.datosTrabajador.puesto;
                card.workAddress.street = this.cardData.datosTrabajador.direccion;
                card.photo.embedFromString(this.logoGEMIVCard, this.logoGEMIVCardType);
                card.saveToFile(this.trabajadorActual);
            }
        },
        onClickCambiarSeccion(seccion) {
            this.cardData.seccionMostrada = seccion;
        },
        onClickMostrarDocumento(documento){
            this.cardData.seccionMostrada = 'mostrarDocumento';
            this.cardData.documentoElegido = documento;
        },


        /*********  Presentación   ************/
        onClickLogoGemi() {
            if ( this.reproducirAudio ) {
                document.getElementById('auIntro').play();
                this.isPlayingIntro = true;
                setTimeout( () => {
                    this.isPlayingIntro = false;
                    this.irUrl('IDCard.html')
                }, 6000);
            }
            else {
                this.irUrl('IDCard.html');
            }


        },
    }

});
